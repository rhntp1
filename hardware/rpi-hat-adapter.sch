EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:rpi-hat-adapter-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "rpi hat adapter"
Date "9 apr 2015"
Rev "1"
Comp "Radio Helsinki"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_6X2 P3
U 1 1 552442D5
P 6150 3200
F 0 "P3" H 6150 3550 60  0000 C CNN
F 1 "GPS_HAT" V 6150 3200 60  0000 C CNN
F 2 "" H 6150 3200 60  0000 C CNN
F 3 "" H 6150 3200 60  0000 C CNN
	1    6150 3200
	0    -1   -1   0   
$EndComp
$Comp
L CONN_2X2 P2
U 1 1 552442E4
P 4750 2300
F 0 "P2" H 4750 2450 50  0000 C CNN
F 1 "P9_1/2/3/4" H 4760 2170 40  0000 C CNN
F 2 "" H 4750 2300 60  0000 C CNN
F 3 "" H 4750 2300 60  0000 C CNN
	1    4750 2300
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K1
U 1 1 55244302
P 4700 4100
F 0 "K1" V 4650 4100 50  0000 C CNN
F 1 "P9_24/26/28" V 4750 4100 40  0000 C CNN
F 2 "" H 4700 4100 60  0000 C CNN
F 3 "" H 4700 4100 60  0000 C CNN
	1    4700 4100
	-1   0    0    -1  
$EndComp
$Comp
L CONN_2 P4
U 1 1 55244325
P 7550 2250
F 0 "P4" V 7500 2250 40  0000 C CNN
F 1 "P8_1/2" V 7600 2250 40  0000 C CNN
F 2 "" H 7550 2250 60  0000 C CNN
F 3 "" H 7550 2250 60  0000 C CNN
	1    7550 2250
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR01
U 1 1 55244343
P 7450 2650
F 0 "#PWR01" H 7450 2650 30  0001 C CNN
F 1 "GND" H 7450 2580 30  0001 C CNN
F 2 "" H 7450 2650 60  0000 C CNN
F 3 "" H 7450 2650 60  0000 C CNN
	1    7450 2650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 55244350
P 7650 2650
F 0 "#PWR02" H 7650 2650 30  0001 C CNN
F 1 "GND" H 7650 2580 30  0001 C CNN
F 2 "" H 7650 2650 60  0000 C CNN
F 3 "" H 7650 2650 60  0000 C CNN
	1    7650 2650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 55244362
P 5200 2250
F 0 "#PWR03" H 5200 2250 30  0001 C CNN
F 1 "GND" H 5200 2180 30  0001 C CNN
F 2 "" H 5200 2250 60  0000 C CNN
F 3 "" H 5200 2250 60  0000 C CNN
	1    5200 2250
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR04
U 1 1 55244379
P 4300 2250
F 0 "#PWR04" H 4300 2250 30  0001 C CNN
F 1 "GND" H 4300 2180 30  0001 C CNN
F 2 "" H 4300 2250 60  0000 C CNN
F 3 "" H 4300 2250 60  0000 C CNN
	1    4300 2250
	0    1    1    0   
$EndComp
$Comp
L +3.3V #PWR05
U 1 1 55244387
P 5200 2350
F 0 "#PWR05" H 5200 2310 30  0001 C CNN
F 1 "+3.3V" H 5200 2460 30  0000 C CNN
F 2 "" H 5200 2350 60  0000 C CNN
F 3 "" H 5200 2350 60  0000 C CNN
	1    5200 2350
	0    1    1    0   
$EndComp
$Comp
L +3.3V #PWR06
U 1 1 55244394
P 4300 2350
F 0 "#PWR06" H 4300 2310 30  0001 C CNN
F 1 "+3.3V" H 4300 2460 30  0000 C CNN
F 2 "" H 4300 2350 60  0000 C CNN
F 3 "" H 4300 2350 60  0000 C CNN
	1    4300 2350
	0    -1   -1   0   
$EndComp
$Comp
L +3.3V #PWR07
U 1 1 552443A4
P 5900 3650
F 0 "#PWR07" H 5900 3610 30  0001 C CNN
F 1 "+3.3V" H 5900 3760 30  0000 C CNN
F 2 "" H 5900 3650 60  0000 C CNN
F 3 "" H 5900 3650 60  0000 C CNN
	1    5900 3650
	-1   0    0    1   
$EndComp
NoConn ~ 6200 3600
NoConn ~ 6300 3600
NoConn ~ 6400 3600
NoConn ~ 6000 2800
NoConn ~ 5900 2800
$Comp
L GND #PWR08
U 1 1 552443AA
P 6100 2750
F 0 "#PWR08" H 6100 2750 30  0001 C CNN
F 1 "GND" H 6100 2680 30  0001 C CNN
F 2 "" H 6100 2750 60  0000 C CNN
F 3 "" H 6100 2750 60  0000 C CNN
	1    6100 2750
	-1   0    0    1   
$EndComp
Text Label 6200 2650 1    60   ~ 0
TxD
Text Label 6300 2650 1    60   ~ 0
RxD
Text Label 6400 2650 1    60   ~ 0
PPS
Text Label 5150 4100 0    60   ~ 0
RxD
Text Label 5150 4000 0    60   ~ 0
TxD
Text Label 5150 4200 0    60   ~ 0
PPS
Wire Wire Line
	7450 2650 7450 2600
Wire Wire Line
	7650 2650 7650 2600
Wire Wire Line
	5200 2350 5150 2350
Wire Wire Line
	5150 2250 5200 2250
Wire Wire Line
	4350 2350 4300 2350
Wire Wire Line
	4300 2250 4350 2250
Wire Wire Line
	6200 2800 6200 2650
Wire Wire Line
	6300 2800 6300 2650
Wire Wire Line
	6400 2800 6400 2650
Wire Wire Line
	5050 4000 5150 4000
Wire Wire Line
	5150 4100 5050 4100
Wire Wire Line
	5050 4200 5150 4200
Wire Wire Line
	5900 3600 5900 3650
Wire Wire Line
	6100 2800 6100 2750
$Comp
L CONN_2 P1
U 1 1 5524450D
P 4650 3350
F 0 "P1" V 4600 3350 40  0000 C CNN
F 1 "P9_19/20" V 4700 3350 40  0000 C CNN
F 2 "" H 4650 3350 60  0000 C CNN
F 3 "" H 4650 3350 60  0000 C CNN
	1    4650 3350
	0    -1   -1   0   
$EndComp
Text Label 6200 3800 0    60   ~ 0
SCL
Text Label 6200 3900 0    60   ~ 0
SDA
Text Label 4800 3750 0    60   ~ 0
SDA
Text Label 4500 3750 2    60   ~ 0
SCL
Wire Wire Line
	6100 3600 6100 3800
Wire Wire Line
	6100 3800 6200 3800
Wire Wire Line
	6200 3900 6000 3900
Wire Wire Line
	6000 3900 6000 3600
Wire Wire Line
	4750 3700 4750 3750
Wire Wire Line
	4750 3750 4800 3750
Wire Wire Line
	4500 3750 4550 3750
Wire Wire Line
	4550 3750 4550 3700
$Comp
L CONN_6 P6
U 1 1 5525C169
P 4150 5150
F 0 "P6" V 4100 5150 60  0000 C CNN
F 1 "debug_serial_ex" V 4200 5150 60  0000 C CNN
F 2 "" H 4150 5150 60  0000 C CNN
F 3 "" H 4150 5150 60  0000 C CNN
	1    4150 5150
	-1   0    0    -1  
$EndComp
$Comp
L CONN_6 P5
U 1 1 55246AB3
P 5150 5150
F 0 "P5" V 5100 5150 60  0000 C CNN
F 1 "debug_serial" V 5200 5150 60  0000 C CNN
F 2 "" H 5150 5150 60  0000 C CNN
F 3 "" H 5150 5150 60  0000 C CNN
	1    5150 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4900 4500 4900
Wire Wire Line
	4500 5000 4800 5000
Wire Wire Line
	4800 5100 4500 5100
Wire Wire Line
	4500 5200 4800 5200
Wire Wire Line
	4800 5300 4500 5300
Wire Wire Line
	4500 5400 4800 5400
$EndSCHEMATC
